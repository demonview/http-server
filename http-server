#! /usr/bin/env escript
-mode(compile).
-export([do/1]).
-include_lib("inets/include/httpd.hrl").

main(Args) -> defaults(), options(Args), httpd().

defaults() ->
    put(port, 8080),
    put(address, "0.0.0.0"),
    put(silent, false),
    put(log, false).

options([]) -> ok;
options(["-h"|_]) -> usage();
options(["--help"|_]) -> usage();
options(["-p",Port|Rest]) ->
    try list_to_integer(Port) catch error:badarg -> usage() end,
    put(port, list_to_integer(Port)),
    options(Rest);
options(["-a",Addr|Rest]) -> put(address, Addr), options(Rest);
options(["-s"|Rest]) -> put(silent, true), options(Rest);
options(["-l"|Rest]) -> put(log, true), options(Rest);
options(_) -> usage().

usage() ->
    io:fwrite("usage: http-server [options]\n\n"
              "options:\n"
              "  -s          Suppress log messages from output\n"
              "  -l          Log to error_log and access_log files\n"
              "  -a <addr>   Address to use [0.0.0.0]\n"
              "  -p <port>   Port to use [8080]\n"
              "\n"
              "  -h --help   Produce this output.\n"
              "\n"
              "Files are served from public/ , if it exists, and otherwise\n"
              "from the working directory.\n"),
    halt(1).

httpd() ->
    {ServerRoot, DocRoot} = dirs(),
    Opts = [{bind_address, get(address)}, {port, get(port)}, {ipfamily, inet},
            {document_root, DocRoot}, {server_root, ServerRoot},
            {server_name, "http_server"}, {modules, modules()},
            {error_log, "error_log"}, {transfer_log, "access_log"}],
    ok = inets:start(),
    {ok, Pid} = inets:start(httpd, Opts),
    case get(silent) of false -> startup(Pid); true -> ok end,
    receive _ -> ok end.

dirs() -> dirs(file:get_cwd(), file:list_dir("public")).
dirs({ok, Cwd}, {ok, _}) -> {Cwd, Cwd ++ "/public/"};
dirs({ok, Cwd}, {error, _}) -> {Cwd, Cwd}.

modules() ->
    Disklog = case get(log) of true -> [mod_log, mod_disk_log]; false -> [] end,
    Log = case get(silent) of true -> []; false -> [?MODULE] end,
    [mod_dir, mod_get, mod_head] ++ Disklog ++ Log.


startup(Pid) ->
    [{_,D},{_,P},{_,B}] = httpd:info(Pid, [document_root, port, bind_address]),
    io:fwrite("~s ~s\n~s\n ~s:~s\nHit CTRL-C to stop the server\n",
              [yellow("Starting up http-server, serving"), cyan(D),
               yellow("Available on:"), green(ip(B)),
               green(integer_to_list(P))]).

do(Mod=#mod{data=Data}) ->
    log(Mod),
    {proceed, Data}.

log(Mod=#mod{data=[{status,{S,_,R}}]}) -> failure(Mod, S, R);
log(Mod) -> success(Mod).

success(#mod{request_line=Request, parsed_header=Headers}) ->
    io:fwrite("[~s] \"~s\" \"~s\"\n",
              [timestamp(), cyan(Request), ua(Headers)]).

failure(#mod{request_line=Request}, Status, Reason) ->
    io:fwrite("[~s] \"~s\" Error (~w): ~p\n",
              [timestamp(), red(Request), Status, Reason]).

ua(Headers) ->
    case lists:keysearch("user-agent", 1, Headers) of
        {value, {_, UA}} -> UA;
        false -> "-"
    end.

timestamp() ->
    {Date={Y,M,D},{H,Mi,S}} = calendar:local_time(),
    Day = calendar:day_of_the_week(Date),
    io_lib:fwrite("~s, ~w ~s ~w ~2..0b:~2..0b:~2..0b",
                  [days(Day), D, months(M), Y, H, Mi, S]).

ip({A,B,C,D}) -> io_lib:fwrite("~b.~b.~b.~b", [A,B,C,D]).

days(1) -> "Mon";
days(2) -> "Tue";
days(3) -> "Wed";
days(4) -> "Thu";
days(5) -> "Fri";
days(6) -> "Sat";
days(7) -> "Sun".

months(1) -> "Jan";
months(2) -> "Feb";
months(3) -> "Mar";
months(4) -> "Apr";
months(5) -> "May";
months(6) -> "Jun";
months(7) -> "Jul";
months(8) -> "Aug";
months(9) -> "Sep";
months(10) -> "Oct";
months(11) -> "Nov";
months(12) -> "Dec".

red(S) -> ["\e[31m",S,"\e[0m"].
green(S) -> ["\e[32m",S,"\e[0m"].
yellow(S) -> ["\e[33m",S,"\e[0m"].
cyan(S) -> ["\e[36m",S,"\e[0m"].
